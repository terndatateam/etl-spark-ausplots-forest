# Spark ETL Ausplots Forest
This repository contains the Ausplots Forest ETL using Apache Spark 3.4.1 with Python >3.7  

## Installation

Install Java 8, 11 or 17 on your computer (needed by Spark).

```
JAVA_HOME=C:\Program Files\Java\jdk-11;
```

Use a virtualenv. Once virtualenv is activated, install the Python dependencies.
```
pip install -r requirements.txt
```

Configure the following env variables:
```
POSTGRES_HOST=localhost # SSH tunnel to postgres cluster
POSTGRES_PORT=8888
POSTGRES_DB=ecoplatform
POSTGRES_USER=ecoplatform
POSTGRES_PASSWORD=password # test postgres cluster
```
If planning to read data directly from Ecoplatform DB in TERN postgres cluster, create a SSH tunnel though bastion.
Execute and keep open this command in a new terminal:
```
ssh -i .ssh/your_private_key -L 8888:10.10.10.10:9999 uqusername@bastion.tern.org.au
```

The postgres driver JAR is needed by Spark to connect to the DB, and it's not included in the pyspark distribution.
Please copy the attached file postgresql-42.2.10.jar to the following directory (path may change in every computer, e.g. different python version):
```
...\etl-spark-ausplots-forests\venv\lib\python3.11\site-packages\pyspark\jars
```

## Example usage
Set up the desired configuration within the file _config.py_.

* Debugging purposes:
    - It may be useful to use a single core to avoid getting multiple output files.

        ```run_on_single_process = True```
        
    - Filter the source dataset to work with handy data. 
    
        ```DEBUG = True```
        
        ```DEBUG_SITE_LOCATION_VISIT = [sites_ids...]```

Run the Spark from you development environment (VSCode, Pycharm) or your terminal running the etl.py script.
```
python etl.py
```

or

Run the ETL as a Spark job, using spark-submit bin.
```
# Only necessary if local machine is a MAC.
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES

# Ensure .env is configured. See .env-template.

# Run a Spark job on the local machine (submitting the job, you can include the path to the postgres driver directly)
spark-submit --driver-class-path postgresql-42.2.10.jar etl.py > output.log
```

## Improvements
A few Python packages can be refactored into their own Python package project. This will be useful because these packages are considered *shared code* between CORVEG and other dataset ETLs.

- The `ands_portal` package.
    - It contains a simple API to download RDF from the ANDS Vocabulary Portal.
- The `validation` package.
    - Contains the generic validation functions for table columns of primitive types (string, int, etc.).
- The `database` package.
    - Contains a simple API to connect to a remote database using a Python PostGreSQL driver and retrieve a table as a Pandas `DataFrame` object.

## Contact
**Javier Sanchez**  
*Software Engineer*  
[j.sanchezgonzalez@uq.edu.au](mailto:j.sanchezgonzalez@uq.edu.au)  

**Edmond Chuc**  
*Software Engineer*  
[e.chuc@uq.edu.au](mailto:e.chuc@uq.edu.au)  
