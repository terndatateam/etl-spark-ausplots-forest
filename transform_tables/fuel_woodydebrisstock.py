import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "fuel_woodydebrisstock"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( SELECT 
                siteid as site_id,
                site_visit_id,
                woodydebrisdiameterclass,
                meanwoodydebrisstock_kilogramspermetresquared,
                samplesizewoodydebrisstock,
                startvisitdate as default_datetime,
                id as deadwood_wds_id,
                id as unique_id
            FROM ausplots_forests.fuel_woodydebrisstock
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID)
            )
            # self.df.show()

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
