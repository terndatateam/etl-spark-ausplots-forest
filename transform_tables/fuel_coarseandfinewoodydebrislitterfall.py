import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, when
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "fuel_coarseandfinewoodydebrislitterfall"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( SELECT 
                siteid AS site_id,
                CONCAT(siteid, '-fuel') AS site_visit_id,
                meancoarsewoodydebrislitterfallmass_tonnesperhectareperyear,
                meancoarsewoodydebrislitterfallturnover,
                meanfinewoodydebrislitterfallmass_tonnesperhectareperyear,
                meanfinewoodydebrislitterfallturnover,
                startsitevisit AS default_datetime,
                siteid AS deadwood_wds_id,
                siteid AS unique_id
            FROM ausplots_forests.fuel_coarseandfinewoodydebrislitterfall
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID)
            )
            # self.df.show()

    def pre_transform(self, errors, warnings) -> None:
        self.df = (
            self.df.withColumn(
                "meancoarsewoodydebrislitterfallmass_tonnesperhectareperyear",
                when(
                    col("meancoarsewoodydebrislitterfallmass_tonnesperhectareperyear")
                    == "NA",
                    None,
                ).otherwise(
                    col("meancoarsewoodydebrislitterfallmass_tonnesperhectareperyear")
                ),
            )
            .withColumn(
                "meancoarsewoodydebrislitterfallturnover",
                when(
                    col("meancoarsewoodydebrislitterfallturnover") == "NA", None
                ).otherwise(col("meancoarsewoodydebrislitterfallturnover")),
            )
            .withColumn(
                "meanfinewoodydebrislitterfallmass_tonnesperhectareperyear",
                when(
                    col("meanfinewoodydebrislitterfallmass_tonnesperhectareperyear")
                    == "NA",
                    None,
                ).otherwise(
                    col("meanfinewoodydebrislitterfallmass_tonnesperhectareperyear")
                ),
            )
            .withColumn(
                "meanfinewoodydebrislitterfallturnover",
                when(
                    col("meanfinewoodydebrislitterfallturnover") == "NA", None
                ).otherwise(col("meanfinewoodydebrislitterfallturnover")),
            )
        )

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
