import datetime
import itertools
import os
import uuid

import pandas as pd
from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType
from rdflib import SKOS, XSD, Literal, Namespace, URIRef
from spark_etl_utils import Transform, add_error, url_encode
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import (
    SITE_LOCATION_ID,
    SITE_LOCATION_VISIT_ID,
    UNIQUE_ID,
    RDFDataset,
    generate_rdf_graph,
    get_vocabulary_concept,
)
from spark_etl_utils.rdf.models import (
    IRI,
    FeatureOfInterest,
    Instant,
    Observation,
    Site,
    SiteVisit,
    Taxon,
    generate_underscore_uri,
)
from whoosh.index import open_dir
from whoosh.qparser import QueryParser

from config import Config
from transform_tables.common import (
    EPBC_CV,
    EPBC_OBS,
    IUCN_CV,
    IUCN_OBS,
    TAXON_OBS,
    create_apc_index,
    post_transform,
)


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        create_apc_index()
        self.ix = open_dir(os.path.join(Config.APP_DIR, Config.HERBARIUM_FILES, "index_files"))
        self.spark = spark
        self.table = "r_specimendeterminations"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE
        self.matched_species = {}
        self.conservation_status = {}

        query = """
          ( SELECT 
                rl."DATE", 
                rl."FIELDSPECIES", 
                rl."VEGBARCODE", 
                rl."DETERMINEDSPECIES", 
                rl."STATEACCESSIONNUMBER", 
                rl."DETERMINER", 
                to_timestamp(rl."DETERMINATIONDATE", 'DD/MM/YYYY HH:MI') as "DETERMINATIONDATE",
                rl."UNCERTAINDETERMINATION",
                rl.site_visit_id,
                rl."SLCODE" as site_id,
                to_date(rl."DATE", 'DD/MM/YYYY') as default_datetime,  
                rl."VEGBARCODE" as unique_id
            from ausplots_forests.r_speciesdeterminations rl
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID)
            )
            # self.df.show()

        decode_udf = udf(lambda val: url_encode(val), StringType())
        self.df = self.df.withColumn("unique_id", decode_udf(self.df["unique_id"]))
        self.df = self.df.withColumn("VEGBARCODE", decode_udf(self.df["VEGBARCODE"]))

    def load_lookup(self):
        return self.spark.sparkContext.broadcast((self.matched_species, self.conservation_status))

    def pre_transform(self, errors, warnings):
        df_conserv = pd.read_csv(Config.CONSERVATION_STATUS_LIST)
        df_conserv = df_conserv.where(pd.notnull(df_conserv), None)

        species_list = self.df.select("DETERMINEDSPECIES").distinct()
        unmatched_species = set()
        for row in species_list.rdd.collect():
            species = row["DETERMINEDSPECIES"]
            if species:
                with self.ix.searcher() as searcher:
                    # (Ausplot taxon -> APC acceptedNameUsage)
                    qp = QueryParser("canonicalName", self.ix.schema)
                    parse_spp = qp.parse(species)
                    results = searcher.search(parse_spp, sortedby="taxonomicStatusPriority")
                    if not results:
                        # (Ausplot taxon -> APC scientificName)
                        warnings.add("[1] Not found in canonical: " + species)
                        qp = QueryParser("scientificName", self.ix.schema)
                        parse_spp = qp.parse(species)
                        results = searcher.search(parse_spp, sortedby="taxonomicStatusPriority")
                        if not results:
                            # (Ausplot taxon -> APC canonicalName)
                            warnings.add("[2] Not found in scientific: " + species)
                            qp = QueryParser("acceptedNameUsage", self.ix.schema)
                            parse_spp = qp.parse(species)
                            results = searcher.search(parse_spp, sortedby="taxonomicStatusPriority")
                            if not results:
                                warnings.add("[3] Not found in accepted: " + species)
                                unmatched_species.add(species)

                    if results:
                        self.matched_species.update({species: dict(results[0])})

                # conservation status
                new_df = df_conserv.loc[df_conserv["Scientific Name"] == species]
                if not new_df.empty:
                    # print(new_df.to_string())
                    iucn_status = None
                    if new_df["IUCN Red List"].iloc[0]:
                        names = new_df["IUCN Red List Listed Names"].iloc[0].split(",")
                        status = new_df["IUCN Red List"].iloc[0].split(",")
                        if len(status) > 1:
                            for i in range(0, len(names)):
                                if species == names[i].strip():
                                    iucn_status = status[i].strip()
                                    # print(iucn_status)
                        else:
                            iucn_status = status[0].strip()

                    self.conservation_status.update(
                        {
                            species: {
                                "epbc": new_df["EPBC Threat Status"].iloc[0],
                                "iucn": iucn_status,
                            }
                        }
                    )

        # print(self.conservation_status)

        with open(
            os.path.join(Config.APP_DIR, "unmatched_species-{}.txt".format(uuid.uuid4())),
            "w",
        ) as fp:
            fp.write("\n".join(unmatched_species))

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        ns = Namespace(namespace_url)
        matched_species = lookup.value[0]
        conservation_status = lookup.value[1]
        vocabs = vocabulary_graph.value
        # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # is exhausted after the first loop).
        rows, rows_cloned = itertools.tee(rows)
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )

        dataset_uri = RDFDataset.generate_uri(Namespace(namespace_url))
        for row in rows_cloned:
            foi_uri = FeatureOfInterest.generate_uri(ns, "site_visit_id,VEGBARCODE", row)
            site_uri = Site.generate_uri(Namespace(namespace_url), row[SITE_LOCATION_ID])
            site_visit_uri = SiteVisit.generate_uri(
                Namespace(namespace_url), row[SITE_LOCATION_VISIT_ID]
            )
            result_time = datetime.datetime.combine(
                row["default_datetime"], datetime.datetime.min.time()
            )
            phenomenom_time = datetime.datetime.combine(
                row["default_datetime"], datetime.datetime.min.time()
            )
            species = matched_species.get(row["DETERMINEDSPECIES"])
            if species:
                taxon = Taxon(
                    uri=URIRef(species["taxonID"]),
                    label=Literal(species["acceptedNameUsage"]),
                    accepted_name_usage=Literal(species["acceptedNameUsage"]),
                    accepted_name_usage_id=Literal(species["acceptedNameUsageID"]),
                    _class=Literal(species["class_"]),
                    family=Literal(species["family"]),
                    higher_classification=Literal(species["higherClassification"]),
                    kingdom=Literal(species["kingdom"]),
                    name_according_to=Literal(species["nameAccordingTo"]),
                    name_according_to_id=Literal(species["nameAccordingToID"]),
                    nomenclatural_code=Literal(species["nomenclaturalCode"]),
                    nomenclatural_status=Literal(species["nomenclaturalStatus"]),
                    scientific_name=Literal(species["scientificName"]),
                    scientific_name_id=Literal(species["scientificNameID"]),
                    scientific_name_authorship=Literal(species["scientificNameAuthorship"]),
                    taxon_concept_id=Literal(species["taxonConceptID"]),
                    taxon_id=Literal(species["taxonID"]),
                    taxonomic_status=Literal(species["taxonomicStatus"]),
                )
                # Accepted name
                tags = f"""{str(row[SITE_LOCATION_ID])},{str(row[SITE_LOCATION_VISIT_ID])},taxon,visual assessment,plant individual"""
                g += Observation(
                    uri=Observation.generate_uri(
                        ns,
                        "r_specimendeterminations",
                        "taxon",
                        row[UNIQUE_ID],
                    ),
                    feature_of_interest=foi_uri,
                    in_dataset=dataset_uri,
                    observed_property=URIRef(
                        "http://linked.data.gov.au/def/tern-cv/70646576-6dc7-4bc5-a9d8-c4c366850df0"
                    ),
                    used_procedure=URIRef(
                        "http://linked.data.gov.au/def/ausplots-cv/ea3cf28f-07e2-49bc-bb79-10f07b5859b7"
                    ),
                    has_result=taxon,
                    has_simple_result=Literal(species["acceptedNameUsage"], datatype=XSD.string),
                    phenomenon_time=Instant(
                        uri=generate_underscore_uri(ns),
                        datetime=Literal(phenomenom_time, datatype=XSD.dateTime),
                    ),
                    result_datetime=Literal(result_time, datatype=XSD.dateTime),
                    has_site=site_uri,
                    has_site_visit=site_visit_uri,
                    ecoplots_tags=Literal(tags, datatype=XSD.string),
                ).g

            # print(row["DETERMINEDSPECIES"])
            cons_species = conservation_status.get(row["DETERMINEDSPECIES"])
            if cons_species:
                # EPBC
                if cons_species["epbc"]:
                    result_uri = get_vocabulary_concept(
                        vocabs, EPBC_CV, SKOS.notation, cons_species["epbc"]
                    )
                    if not result_uri:
                        add_error(
                            errors,
                            "Concept (IRI) was not found for {}: {}".format(
                                "EPBC Threat Status", cons_species["epbc"]
                            ),
                        )
                    else:
                        result = IRI(uri=generate_underscore_uri(ns), value=URIRef(result_uri))
                        tags = f"""{str(row[SITE_LOCATION_ID])},{str(row[SITE_LOCATION_VISIT_ID])},EPBC act conservastion status,visual assessment,plant individual"""
                        g += Observation(
                            uri=Observation.generate_uri(
                                ns,
                                "r_specimendeterminations",
                                "epbc",
                                row[UNIQUE_ID],
                            ),
                            feature_of_interest=foi_uri,
                            in_dataset=dataset_uri,
                            observed_property=EPBC_OBS,
                            used_procedure=URIRef(
                                "http://linked.data.gov.au/def/ausplots-cv/f30bb2ac-4ea7-4836-8812-134ea29cbb8f"
                            ),
                            has_result=result,
                            has_simple_result=Literal(result.value),
                            phenomenon_time=Instant(
                                uri=generate_underscore_uri(ns),
                                datetime=Literal(phenomenom_time, datatype=XSD.dateTime),
                            ),
                            result_datetime=Literal(result_time, datatype=XSD.dateTime),
                            has_site=site_uri,
                            has_site_visit=site_visit_uri,
                            ecoplots_tags=Literal(tags, datatype=XSD.string),
                        ).g
                # IUCN
                if cons_species["iucn"]:
                    result_uri = get_vocabulary_concept(
                        vocabs, IUCN_CV, SKOS.notation, cons_species["iucn"]
                    )
                    if not result_uri:
                        add_error(
                            errors,
                            "Concept (IRI) was not found for {}: {}".format(
                                "IUCN Red List", cons_species["iucn"]
                            ),
                        )
                    else:
                        result = IRI(uri=generate_underscore_uri(ns), value=URIRef(result_uri))
                        tags = f"""{str(row[SITE_LOCATION_ID])},{str(row[SITE_LOCATION_VISIT_ID])},IUCN conservation status,visual assessment,plant individual"""
                        g += Observation(
                            uri=Observation.generate_uri(
                                ns,
                                "r_specimendeterminations",
                                "iucn",
                                row[UNIQUE_ID],
                            ),
                            feature_of_interest=foi_uri,
                            in_dataset=dataset_uri,
                            observed_property=IUCN_OBS,
                            used_procedure=URIRef(
                                "http://linked.data.gov.au/def/ausplots-cv/f30bb2ac-4ea7-4836-8812-134ea29cbb8f"
                            ),
                            has_result=result,
                            has_simple_result=URIRef(result.value),
                            phenomenon_time=Instant(
                                uri=generate_underscore_uri(ns),
                                datetime=Literal(phenomenom_time, datatype=XSD.dateTime),
                            ),
                            result_datetime=Literal(result_time, datatype=XSD.dateTime),
                            has_site=site_uri,
                            has_site_visit=site_visit_uri,
                            ecoplots_tags=Literal(tags, datatype=XSD.string),
                        ).g

        post_transform(g, ontology.value, table_name)
