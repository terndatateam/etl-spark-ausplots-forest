import itertools
from datetime import datetime

import psycopg2 as pg
from psycopg2._psycopg import AsIs
from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import (
    ArrayType,
    BooleanType,
    DateType,
    FloatType,
    IntegerType,
    StringType,
    StructField,
    StructType,
)
from rdflib import DCTERMS, XSD, Literal, Namespace, URIRef
from spark_etl_utils import Transform, add_error
from spark_etl_utils.database import get_db_query_pg, get_db_table_pandas
from spark_etl_utils.rdf import (
    SITE_LOCATION_ID,
    SITE_LOCATION_VISIT_ID,
    SITE_TYPE_SITE,
    SW_POINT_TYPE,
    generate_rdf_graph,
)
from spark_etl_utils.rdf.models import (
    Point,
    RDFDataset,
    Site,
    SiteVisit,
    generate_underscore_uri,
)
from tern_rdf.namespace_bindings import GEOSPARQL

from config import Config
from transform_tables.common import post_transform

TERN_SITE_SCHEMA = StructType(
    [
        StructField("ausplots_id", StringType(), True),
        StructField("site_name", StringType(), True),
        StructField("Date", StringType(), True),
        StructField("description", StringType(), True),
        StructField("latitude", FloatType(), True),
        StructField("longitude", FloatType(), True),
        StructField("elevation", IntegerType(), True),
        StructField("new_plot", BooleanType(), True),
        StructField("existing_plot", StringType(), True),
        StructField("custodian", StringType(), True),
        StructField("fire_severity", StringType(), True),
        StructField("max_tree_height", IntegerType(), True),
        StructField("target_species", StringType(), True),
        StructField("target_species_growth_stage", StringType(), True),
        StructField("cut_stumps", StringType(), True),
        StructField("understorey", StringType(), True),
        StructField("disturbance", StringType(), True),
        StructField("bioregion_name", StringType(), True),
        StructField("bioregion_full_name", StringType(), True),
        StructField("plot_is_permanently_marked", BooleanType(), True),
        StructField("plot_is_aligned_to_grid", BooleanType(), True),
        StructField("plot_is_100m_by_100m", BooleanType(), True),
        StructField("site_slope", IntegerType(), True),
        StructField("site_slope_text", StringType(), True),
        StructField("site_aspect", IntegerType(), True),
        StructField("plot_dimensions", StringType(), True),
        StructField("landform_pattern", StringType(), True),
        StructField("landform_element", StringType(), True),
        StructField("site_visit_id", StringType(), True),
        StructField("site_id", StringType(), True),
        StructField("vegetation_disturbance_id", StringType(), True),
        StructField("landform_id", StringType(), True),
        StructField("land_surface_id", StringType(), True),
        StructField("plant_population_id", StringType(), True),
        StructField("default_datetime", DateType(), True),
        StructField("unique_id", StringType(), True),
        StructField("regions", ArrayType(StringType()), True),
    ]
)

PLOT_SHAPE_SQUARE = "http://linked.data.gov.au/def/tern-cv/b2e07a7f-942e-44d9-9fd6-f0cee7be103d"
PLOT_SHAPE_RECTANGLE = "http://linked.data.gov.au/def/tern-cv/c4215f3c-1f25-4c1c-b910-27027848fc8f"


def get_plot_dimensions(dimensions):
    if dimensions in (
        "100 x 100m.",
        "'100  x  100'",
        "100m x 100m.",
        "100 X 100m.",
        "100 x 100m",
        "100 x 100",
        "100m. x 100m.",
        "100  x  100 m",
        "100 x 100 m.",
    ):
        return 100.0, 100.0, 100.0 * 100.0, PLOT_SHAPE_SQUARE
    elif dimensions in (
        "'200  x  50m.'",
        "200 m. x 50 m.",
        "200  x  50 m.",
        "'200  x  50 m.'",
    ):
        return 50.0, 200.0, 50.0 * 200.0, PLOT_SHAPE_RECTANGLE
    elif dimensions == "100 x 1600m.":
        return 100.0, 1600.0, 100.0 * 1600.0, PLOT_SHAPE_RECTANGLE
    elif dimensions == "100 x 160m.":
        return 100.0, 160.0, 100.0 * 160.0, PLOT_SHAPE_RECTANGLE
    elif dimensions == "101 x 100m.":
        return 101.0, 100.0, 101.0 * 100.0, PLOT_SHAPE_RECTANGLE
    else:
        return 100.0, 100.0, 100.0 * 100.0, PLOT_SHAPE_SQUARE


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "data_study_location"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( 
            select 
                sl.*,
                slv.site_visit_id, 
                slv.visit_start + '00:00:01'::time as visit_start,
                slv.visit_end + '00:00:01'::time as visit_end,
                sl.ausplot_id as site_id,
                sl.ausplot_id as vegetation_disturbance_id,
                sl.ausplot_id as landform_id,
                sl.ausplot_id as land_surface_id,
                sl.ausplot_id as plant_population_id,
                slv.visit_start as default_datetime,  
                slv.site_visit_id as unique_id,
                sl.ausplot_id as site_attr_unique_id
            from ausplots_forests.data_study_location sl 
                join ausplots_forests.tern_r_studylocationvisit slv on sl.ausplot_id = slv.site_id
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_LOCATION_ID))
            # self.df.show()

    def pre_transform(self, errors, warnings) -> None:
        get_plot_dimensions_width_udf = udf(lambda z: get_plot_dimensions(z)[0], FloatType())
        get_plot_dimensions_length_udf = udf(lambda z: get_plot_dimensions(z)[1], FloatType())
        get_plot_dimensions_area_udf = udf(lambda z: get_plot_dimensions(z)[2], FloatType())
        get_plot_dimensions_shape_udf = udf(lambda z: get_plot_dimensions(z)[3], StringType())
        df_derived = self.df.withColumn(
            "plot_width", get_plot_dimensions_width_udf("plot_dimensions")
        )
        df_derived = df_derived.withColumn(
            "plot_length", get_plot_dimensions_length_udf("plot_dimensions")
        )
        df_derived = df_derived.withColumn(
            "plot_area", get_plot_dimensions_area_udf("plot_dimensions")
        )
        df_derived = df_derived.withColumn(
            "plot_shape", get_plot_dimensions_shape_udf("plot_dimensions")
        )
        self.df = df_derived

        ##--------------------------------------------------------------------------------------------------------------
        # No generalisation needed. No data needs to be generalised. One-off dataset
        ##--------------------------------------------------------------------------------------------------------------

        # debug_q = f"""where ausplot_id in ('{"','".join(Config.DEBUG_SITE_LOCATION_ID)}')""" if Config.DEBUG else ""
        # query = f"""
        # (
        #     select distinct ausplot_id as site_id, latitude , longitude  from ausplots_forests.data_study_location {debug_q}
        # ) q
        # """
        # print(query)
        # df_sites = get_db_query_pg(
        #     self.spark,
        #     self.db_host,
        #     self.db_port,
        #     self.db_name,
        #     query,
        #     self.db_username,
        #     self.db_password,
        # )
        # conn = pg.connect(host=self.db_host, port=self.db_port, dbname="ultimate_feature_of_interest",
        #                   user=self.db_username, password=self.db_password)
        # src_cursor = conn.cursor()
        # site_state = {}
        # for row in df_sites.rdd.collect():
        #     src_cursor.execute(
        #         "select %(value)s from %(table_name)s where ST_Intersects(geom, ST_SetSRID(ST_MakePoint(%(lon)s, %(lat)s), 3577))",
        #         {
        #             "table_name": AsIs("states_territories"),
        #             "value": AsIs(Config.ufoi_list["states_territories"][1]),
        #             "lon": row["longitude"],
        #             "lat": row["latitude"],
        #         },
        #     )
        #     result = src_cursor.fetchone()
        #     if result is not None:
        #         site_state.update({row["site_id"]: state_from_uri(result[0])})
        #
        # print(site_state)
        #
        # debug_q1 = f"""and rl.slcode in ('{"','".join(Config.DEBUG_SITE_LOCATION_ID)}')""" if Config.DEBUG else ""
        # debug_q2 = f"""and rs."SLCODE" in ('{"','".join(Config.DEBUG_SITE_LOCATION_ID)}')""" if Config.DEBUG else ""
        # debug_q3 = f"""and tvsc.site_id in ('{"','".join(Config.DEBUG_SITE_LOCATION_ID)}')""" if Config.DEBUG else ""
        # query = f"""
        # (
        #     select distinct * from (
        #         select distinct rl.determinedspecies, rl.slcode as site_id from ausplots_forests.r_largetreesurvey rl where rl.determinedspecies is not null {debug_q1}
        #         union
        #         select distinct rs."DETERMINEDSPECIES", rs."SLCODE" as site_id from ausplots_forests.r_speciesdeterminations rs where rs."DETERMINEDSPECIES" is not null {debug_q2}
        #         union
        #         select distinct tvsc.spp, site_id from ausplots_forests.tern_vegetative_species_composition tvsc where spp is not null {debug_q3}
        #     ) q2
        # ) q
        # """
        # print(query)
        # df_species = get_db_query_pg(
        #     self.spark,
        #     self.db_host,
        #     self.db_port,
        #     self.db_name,
        #     query,
        #     self.db_username,
        #     self.db_password,
        # )
        # # df_species.show()
        # # spps = []
        # # for row in df_species.rdd.collect():
        # #     spps.append(row["determinedspecies"])
        # # print(spps)
        #
        # response = urllib.request.urlopen(Config.CONSERVATION_STATUS_LIST).read()
        # tree = ET.fromstring(response)
        # site_gen = {}
        # for row in df_species.rdd.collect():
        #     species = tree.findall(f"sensitiveSpecies[@name='{row['determinedspecies']}']")
        #     for s in species:
        #         print(species)
        #         instances = s.find("instances").findall("conservationInstance")
        #         for i in instances:
        #             print(row["site_id"])
        #             print(i.attrib["zone"])
        #             print(site_state[row["site_id"]])
        #             if i.attrib["zone"] == site_state[row["site_id"]]:
        #                 print("yeahhhhh")
        #                 # Site must be generalised
        #                 if row["site_id"] in site_gen:
        #                     previous_set = site_gen[row["site_id"]]
        #                     previous_set.add(i.attrib["generalisation"])
        #                 else:
        #                     site_gen.update({row["site_id"]: set([i.attrib["generalisation"]])})
        # print(site_gen)
        # df_site_gen = self.spark.createDataFrame(data=site_gen, schema=["site_id", "generalisation"])
        # df_site_gen.printSchema()
        # df_site_gen.show(truncate=False)

    def load_lookup(self):
        ufoi_db = {
            "host": self.db_host,
            "port": self.db_port,
            "dbname": "ultimate_feature_of_interest",
            "user": self.db_username,
            "password": self.db_password,
        }
        dataset_version = get_db_table_pandas(
            self.db_host,
            self.db_port,
            self.db_name,
            "r_ecoplatform_metadata",
            self.db_username,
            self.db_password,
            Config.DATASET,
            query="select load_date from {}.r_ecoplatform_metadata".format(Config.DATASET),
        ).iloc[0, 0]

        return self.spark.sparkContext.broadcast([dataset_version, ufoi_db])

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # is exhausted after the first loop).
        ns = Namespace(namespace_url)
        rows, rows_cloned = itertools.tee(rows)
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        dataset_uri = RDFDataset.generate_uri(Namespace(namespace_url))
        g += RDFDataset(
            uri=dataset_uri,
            title=Literal("TERN AusPlots Forest"),
            description=Literal(
                "<div><p>The TERN AusPlots Forest Monitoring Network is a continental scale plot based monitoring "
                "network that was established to improve our understanding of tree growth, forest productivity and "
                "carbon dynamics in tall eucalypt forests in relation to continental scale environmental gradients. "
                "The permanent plot network provided here supports the infrastructure and data for all aspects of "
                "forest dynamics over long periods of time. Between October 2014 and March 2015, fuel load surveys "
                "were conducted in the 48 large 1 ha Ausplots Forest Monitoring Network plots in mature, highly "
                "productive tall eucalypt forests across the Australian continent.</p><p>The dataset comprises data "
                "from the first survey of ~24,000 large trees (>10 cm diameter at breast height; DBH) within 48 1 ha "
                "forest monitoring plots established across Australia between 2011 and "
                "2015. Data includes: [1] Site identifiers (ID and Site Name); [2] Plot Establishment Dates; [3] Tree "
                "identifiers and descriptors (ID, Species, Status, Growth Stage, Crown Class); [4] Tree measurements "
                "(Diameter, Point of Measurement, Height, Location); [5] Comments and ancillary information; and [6] "
                "List of Metagenomic Sample Identifiers.</p><p>The forest fuel survey dataset comprises site-level "
                "summary data from the well-designed fuel load surveys across 48 AusPlots Forests- 1-ha monitoring "
                "plots across Australia. Data presented here includes: [1] Site identifiers (ID and Site Name) and "
                "site location and site-specific notes from fuel survey campaign; [2] site survey dates (start date "
                "and end date); [3] Site climatic information (air temperature and relative humidity); [4] Average "
                "height of plants and the stem densities in those sites; [5] Fuel bed biomass measurements that "
                "include live or dead grass, shrub, vines cover; [6] Litter, Fine Woody and Coarse Woody Debris stocks "
                "and production; [7] Soil Nutrient concentration (Soil Carbon, Soil Hydrogen and Soil Nitrogen "
                "contents); [8] Duff depth and cover.</p></div>"
            ),
            issued=Literal(lookup.value[0], datatype=XSD.date),
            # creator=URIRef(
            #     "https://w3id.org/tern/resources/6be8678c-f354-47da-9667-46daf2a23ac7"
            # ),
            citation=Literal(
                "Wood, S., Stephens, H., Bowman, D. (2020): TERN AusPlots Forest Monitoring Network - Large "
                "Tree Survey - 2012-2015. Version 2.0. Terrestrial Ecosystem Research Network. (Dataset). "
                "https://doi.org/10.4227/05/552486484985D"
            ),
            # publisher=URIRef(
            #     "https://w3id.org/tern/resources/a083902d-d821-41be-b663-1d7cb33eea66"
            # ),
        ).g

        g.add(
            (
                dataset_uri,
                DCTERMS.bibliographicCitation,
                Literal(
                    "Bowman, D., Ebsworth, E. (2022): AusPlots Forest Fuel Survey "
                    "site-level data summary, 2014 - 2015. Version 1.0.0. Terrestrial Ecosystem Research Network. "
                    "(Dataset). https://doi.org/10.25901/efnh-sk06"
                ),
            )
        )

        ufoi_db = lookup.value[1]
        conn = pg.connect(
            host=ufoi_db["host"],
            port=ufoi_db["port"],
            dbname=ufoi_db["dbname"],
            user=ufoi_db["user"],
            password=ufoi_db["password"],
        )
        src_cursor = conn.cursor()
        site_ids = set()
        for row in rows_cloned:
            site_uri = Site.generate_uri(Namespace(namespace_url), row[SITE_LOCATION_ID])
            site_visit_uri = SiteVisit.generate_uri(
                Namespace(namespace_url), row[SITE_LOCATION_VISIT_ID]
            )
            try:
                if row[SITE_LOCATION_ID] not in site_ids:
                    site_ids.add(row[SITE_LOCATION_ID])
                    established_date = datetime.strptime(row["Date"], "%d/%m/%Y %H:%M").date()
                    g += Site(
                        uri=site_uri,
                        identifier=Literal(row[SITE_LOCATION_ID], datatype=XSD.string),
                        in_dataset=dataset_uri,
                        date_commissioned=Literal(established_date, datatype=XSD.date),
                        has_site_visit=site_visit_uri,
                        location_description=Literal(row["description"], datatype=XSD.string),
                        feature_type=URIRef(SITE_TYPE_SITE),
                    ).g

                    lat = row["latitude"]
                    long = row["longitude"]
                    alt = row["elevation"]

                    point_uri = URIRef(generate_underscore_uri(ns))
                    g.add((site_uri, GEOSPARQL.hasGeometry, point_uri))
                    g += Point(
                        uri=point_uri,
                        as_wkt=Literal(
                            "POINT({} {})".format(round(long, 8), round(lat, 8)),
                            datatype=GEOSPARQL.wktLiteral,
                        ),
                        latitude=Literal(lat, datatype=XSD.double),
                        longitude=Literal(long, datatype=XSD.double),
                        elevation=Literal(alt, datatype=XSD.integer) if alt else None,
                        point_type=URIRef(SW_POINT_TYPE),
                    ).g

                    # Calculate regions from TERN PostGIS
                    i = 1
                    for ufoi_key in Config.ufoi_list.keys():
                        src_cursor.execute(
                            "select %(value)s from %(table_name)s where ST_Intersects(geom, ST_SetSRID(ST_MakePoint(%(lon)s, %(lat)s), 3577))",
                            {
                                "table_name": AsIs(ufoi_key),
                                "value": AsIs(Config.ufoi_list[ufoi_key][1]),
                                "lon": long,
                                "lat": lat,
                            },
                        )
                        result = src_cursor.fetchone()
                        if result is not None:
                            region_parent = Config.ufoi_list[ufoi_key][0]
                            region_item = result[0]
                            g.add(
                                (
                                    site_uri,
                                    GEOSPARQL.sfWithin,
                                    URIRef("{}{}".format(region_parent, region_item)),
                                )
                            )
                            i += 1

                start_date = datetime.strptime(
                    row["visit_start"].strftime("%Y-%m-%d %H:%M:%S"),
                    "%Y-%m-%d %H:%M:%S",
                )
                g += SiteVisit(
                    uri=site_visit_uri,
                    identifier=Literal(start_date.strftime("%Y%m%d"), datatype=XSD.string),
                    in_dataset=dataset_uri,
                    has_site=site_uri,
                    started_at_time=Literal(row["visit_start"], datatype=XSD.dateTime),
                    ended_at_time=Literal(row["visit_end"], datatype=XSD.dateTime),
                ).g

            except Exception as e:
                print(e)
                add_error(
                    errors,
                    "Site Visit dates missing or wrong format for SITE_LOCATION_VISIT_ID={}.".format(
                        row[SITE_LOCATION_VISIT_ID]
                    ),
                )

        src_cursor.close()
        post_transform(g, ontology.value, table_name)
