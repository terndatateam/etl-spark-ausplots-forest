import itertools

from pyspark import Broadcast, Accumulator
from pyspark.sql import SparkSession
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):

    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = 'tern_fuelsdata_litterfall'
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          (  select 
                fdlf.site_id as parent_site_id,
                fdlf.trap_location_x,
                fdlf.trap_location_y,
                fdlf.coarse_fuel_litterfall_mass_g,
                fdlf.fine_fuel_litterfall_mass_g,
                fdlf.notes,
                fdlf.time_period_days,
                to_date(fdlf.date_collected, 'DD/MM/YYYY')as date_collected,
                to_date(fdlf.date_deployed, 'DD/MM/YYYY')as date_deployed,
                slv.visit_start as default_datetime,
                concat_ws('_', slv.site_id, fdlf.transect) as site_id,
                fdlf.id as unique_id,
                fdlf.id as litterfall_id,
                slv.site_visit_id
            from ausplots_forests.tern_fuelsdata_litterfall fdlf join ausplots_forests.tern_r_studylocationvisit_fuel slv on fdlf.site_id = slv.site_id
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df['site_visit_id'].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID))
            # self.df.show()

    @staticmethod
    def transform(rows: itertools.chain, dataset: str, namespace_url: str, table_name: str,
                  vocabulary_mappings: Broadcast, vocabulary_graph: Broadcast, errors: Accumulator,
                  lookup: Broadcast = None, ontology: Broadcast = None) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
        )
        post_transform(g, ontology.value, table_name)
