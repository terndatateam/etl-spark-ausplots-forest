import itertools

from pyspark import Broadcast, Accumulator
from pyspark.sql import SparkSession
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):

    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = 'tern_fuelsdata_finefuels'
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                fdff.site_id as parent_site_id,
                fdff.matter_type,
                fdff.mass_grms,
                --fdff.quadrat_length_m,
                --fdff.quadrat_width_m,
                slv.visit_start as default_datetime,
                concat_ws('_', slv.site_visit_id, fdff.transect, quadrat, fdff.id) as unique_id,
                -- concat_ws('_', slv.site_id, fdff.transect) as transect_id,
                concat_ws('_', slv.site_id, fdff.transect, quadrat) as site_id,
                fdff.id as finefuel_id,
                slv.site_visit_id
            from ausplots_forests.tern_fuelsdata_finefuels fdff join ausplots_forests.tern_r_studylocationvisit_fuel slv on fdff.site_id = slv.site_id
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df['site_visit_id'].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID))
            # self.df.show()

    @staticmethod
    def transform(rows: itertools.chain, dataset: str, namespace_url: str, table_name: str,
                  vocabulary_mappings: Broadcast, vocabulary_graph: Broadcast, errors: Accumulator,
                  lookup: Broadcast = None, ontology: Broadcast = None) -> None:
        #
        # ns = Namespace(namespace_url)
        # # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # # is exhausted after the first loop).
        # rows, rows_cloned = itertools.tee(rows)

        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
        )

        # dataset_uri = RDFDataset.generate_uri(ns)
        # for row in rows_cloned:
        #     # Create transects
        #     transect = Transect(
        #         uri=Transect.generate_uri(ns, row['transect_id']),
        #         identifier=Literal(row["transect_id"], datatype=XSD.string),
        #         in_dataset=dataset_uri,
        #         is_sample_of=Site.generate_uri(ns, row["parent_site_id"]),
        #         feature_type=URIRef(SITE_TYPE_TRANSECT),
        #         has_site_visit=SiteVisit.generate_uri(ns, row["site_visit_id"]),
        #         # transect_direction=Literal(row["transect"], datatype=XSD.string),
        #     )
        #     g += transect.g
        #
        #     quadrat_id = Site.generate_uri(ns, row['site_id'])
        #     quadrat = Site(
        #         uri=quadrat_id,
        #         identifier=Literal(row["site_id"], datatype=XSD.string),
        #         in_dataset=dataset_uri,
        #         feature_type=URIRef(SITE_TYPE_QUADRAT),
        #         is_sample_of=Transect.generate_uri(ns, row['transect_id']),
        #         has_site_visit=SiteVisit.generate_uri(ns, row["site_visit_id"]),
        #         # has_site_visit=site_visit_uri,
        #     )
        #     g += quadrat.g
        #
        #     g += Attribute(
        #         uri=Attribute.generate_uri(
        #             Namespace(namespace_url),
        #             "tern_fuelsdata_finefuels",
        #             "quadrat_length_m",
        #             row[UNIQUE_ID],
        #         ),
        #         in_dataset=dataset_uri,
        #         is_attribute_of=quadrat_id,
        #         attribute=URIRef(
        #             "http://linked.data.gov.au/def/tern-cv/c053b5c2-17d8-4218-92c9-7ab9c1c808a4"
        #         ),
        #         has_value=Float(
        #             generate_underscore_uri(ns),
        #             Literal(row["quadrat_length_m"], datatype=XSD.decimal),
        #             URIRef("http://qudt.org/vocab/unit/M")
        #         ),
        #         has_simple_value=Literal(row["quadrat_length_m"], datatype=XSD.decimal),
        #     ).g
        #
        #     g += Attribute(
        #         uri=Attribute.generate_uri(
        #             Namespace(namespace_url),
        #             "tern_fuelsdata_finefuels",
        #             "quadrat_width_m",
        #             row[UNIQUE_ID],
        #         ),
        #         in_dataset=dataset_uri,
        #         is_attribute_of=quadrat_id,
        #         attribute=URIRef(
        #             "http://linked.data.gov.au/def/tern-cv/4405cb8b-ef31-465e-a1cf-7b266d26901d"
        #         ),
        #         has_value=Float(
        #             generate_underscore_uri(ns),
        #             Literal(row["quadrat_width_m"], datatype=XSD.decimal),
        #             URIRef("http://qudt.org/vocab/unit/M")
        #         ),
        #         has_simple_value=Literal(row["quadrat_width_m"], datatype=XSD.decimal),
        #     ).g

        post_transform(g, ontology.value, table_name)
