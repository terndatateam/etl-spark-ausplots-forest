import itertools
from datetime import datetime

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from rdflib import XSD, Literal, Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import SITE_LOCATION_ID, SITE_LOCATION_VISIT_ID
from spark_etl_utils.rdf.models import RDFDataset, Site, SiteVisit
from tern_rdf.namespace_bindings import TernRdf

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "fuel_sitevisit"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( 
            select 
                fsv.siteid as site_id,
                fsv.sitevisitid as site_visit_id, 
                fsv.startvisitdate + '00:00:01'::time as visit_start,
                fsv.endvisitdate + '00:00:01'::time as visit_end             
            from ausplots_forests.fuel_sitevisit fsv
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_LOCATION_ID))
            # self.df.show()

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # is exhausted after the first loop).
        ns = Namespace(namespace_url)
        rows, rows_cloned = itertools.tee(rows)

        g = TernRdf.Graph([dataset.upper()])

        dataset_uri = RDFDataset.generate_uri(ns)

        for row in rows_cloned:
            site_uri = Site.generate_uri(ns, row[SITE_LOCATION_ID])
            site_visit_uri = SiteVisit.generate_uri(ns, row[SITE_LOCATION_VISIT_ID])
            start_date = datetime.strptime(
                row["visit_start"].strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S"
            )
            start_date = datetime.strptime(
                row["visit_start"].strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S"
            )
            g += SiteVisit(
                uri=site_visit_uri,
                identifier=Literal(start_date.strftime("%Y%m%d"), datatype=XSD.string),
                in_dataset=dataset_uri,
                has_site=site_uri,
                started_at_time=Literal(row["visit_start"], datatype=XSD.dateTime),
                ended_at_time=Literal(row["visit_end"], datatype=XSD.dateTime),
            ).g

        post_transform(g, ontology.value, table_name)
