import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from rdflib import XSD, Literal, Namespace, URIRef
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import (
    SITE_LOCATION_ID,
    SITE_LOCATION_VISIT_ID,
    SITE_TYPE_QUADRAT,
    SITE_TYPE_TRANSECT,
)
from spark_etl_utils.rdf.models import (
    Attribute,
    Float,
    RDFDataset,
    Site,
    SiteVisit,
    Transect,
    generate_underscore_uri,
)
from tern_rdf.namespace_bindings import TernRdf

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "transect_quadract"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( 
            select distinct q2.site_id, transect, quadrat, site_visit_id  from (
                select site_id, transect, NULL::int4 as quadrat
                from ausplots_forests.tern_soil_samples
                union
                select site_id, transect, NULL::int4 as quadrat
                from ausplots_forests.tern_fuelsdata_fuelheight
                union
                select site_id, transect, NULL::int4 as quadrat
                from ausplots_forests.tern_fuelsdata_litterfall
                union
                select site_id, transect, quadrat
                from ausplots_forests.tern_fuelsdata_finefuels
                union
                select site_id, transect, quadrat
                from ausplots_forests.tern_fuelsdata_fuelcover
                union
                select site_id, transect, NULL::int4 as quadrat
                from ausplots_forests.tern_elevatedfuelstemdensity
                union
                select site_id, transect, NULL::int4 as quadrat
                from ausplots_forests.tern_elevatedfuelht
            ) q2 join ausplots_forests.tern_r_studylocationvisit_fuel slv on q2.site_id = slv.site_id
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_LOCATION_ID))
            # self.df.show()

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # is exhausted after the first loop).
        ns = Namespace(namespace_url)
        g = TernRdf.Graph([dataset.upper()])

        dataset_uri = RDFDataset.generate_uri(Namespace(namespace_url))

        transects = set()
        quadrats = set()

        for row in rows:
            site_uri = Site.generate_uri(ns, row[SITE_LOCATION_ID])
            site_visit_uri = SiteVisit.generate_uri(ns, row[SITE_LOCATION_VISIT_ID])
            transect_id = Transect.generate_uri(ns, f"{row[SITE_LOCATION_ID]}_{row['transect']}")

            if f"{row[SITE_LOCATION_ID]}_{row['transect']}" not in transects:
                # Create transects
                transect = Transect(
                    uri=transect_id,
                    identifier=Literal(
                        f"{row[SITE_LOCATION_ID]}_{row['transect']}",
                        datatype=XSD.string,
                    ),
                    in_dataset=dataset_uri,
                    is_sample_of=site_uri,
                    feature_type=URIRef(SITE_TYPE_TRANSECT),
                    has_site_visit=site_visit_uri,
                )
                g += transect.g
                transects.add(f"{row[SITE_LOCATION_VISIT_ID]}_{row['transect']}")

            if row["quadrat"]:
                quadrat_id = Site.generate_uri(
                    ns, f"{row[SITE_LOCATION_ID]}_{row['transect']}_{row['quadrat']}"
                )
                if f"{row[SITE_LOCATION_ID]}_{row['transect']}_{row['quadrat']}" not in quadrats:
                    quadrat = Site(
                        uri=quadrat_id,
                        identifier=Literal(
                            f"{row[SITE_LOCATION_ID]}_{row['transect']}_{row['quadrat']}",
                            datatype=XSD.string,
                        ),
                        in_dataset=dataset_uri,
                        feature_type=URIRef(SITE_TYPE_QUADRAT),
                        is_sample_of=transect_id,
                        has_site_visit=site_visit_uri,
                    )
                    g += quadrat.g

                    g += Attribute(
                        uri=Attribute.generate_uri(
                            Namespace(namespace_url),
                            "quadrats",
                            "quadrat_length_m",
                            f"{row[SITE_LOCATION_ID]}_{row['transect']}_{row['quadrat']}_1",
                        ),
                        in_dataset=dataset_uri,
                        is_attribute_of=quadrat_id,
                        attribute=URIRef(
                            "http://linked.data.gov.au/def/tern-cv/c053b5c2-17d8-4218-92c9-7ab9c1c808a4"
                        ),
                        has_value=Float(
                            generate_underscore_uri(ns),
                            Literal(1, datatype=XSD.decimal),
                            URIRef("http://qudt.org/vocab/unit/M"),
                        ),
                        has_simple_value=Literal(1, datatype=XSD.decimal),
                    ).g

                    g += Attribute(
                        uri=Attribute.generate_uri(
                            Namespace(namespace_url),
                            "quadrats",
                            "quadrat_width_m",
                            f"{row[SITE_LOCATION_ID]}_{row['transect']}_{row['quadrat']}_2",
                        ),
                        in_dataset=dataset_uri,
                        is_attribute_of=quadrat_id,
                        attribute=URIRef(
                            "http://linked.data.gov.au/def/tern-cv/4405cb8b-ef31-465e-a1cf-7b266d26901d"
                        ),
                        has_value=Float(
                            generate_underscore_uri(ns),
                            Literal(1, datatype=XSD.decimal),
                            URIRef("http://qudt.org/vocab/unit/M"),
                        ),
                        has_simple_value=Literal(1, datatype=XSD.decimal),
                    ).g
                    quadrats.add(f"{row[SITE_LOCATION_ID]}_{row['transect']}_{row['quadrat']}")

        post_transform(g, ontology.value, table_name)
