FROM python:3.7.11-slim-buster

RUN apt-get update && apt-get install -y \
    bash \
    build-essential

COPY requirements.txt .
RUN pip install --no-cache -r requirements.txt

RUN mkdir /etl
RUN mkdir -p /home/ec2-user/.ssh/
VOLUME /etl
WORKDIR /etl