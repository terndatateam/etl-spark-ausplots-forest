import os
import time
import traceback

from pyspark import SparkContext
from pyspark.sql import SparkSession
from rdflib import Graph
from spark_etl_utils import ListAccumulator, parse_vocab_from_url, write_to_disk
from spark_etl_utils.citation.geonetwork_citation import (
    fix_namespace,
    get_party_from_metadata_record,
)
from spark_etl_utils.rdf.vocabulary_mappings import upload_vocabulary_mappings
from tern_rdf import AUSPLOTS_FOREST_BINDINGS, TernRdf

from config import Config
from transform_tables.common import load_ontology, perform_transform

if __name__ == "__main__":
    start_time = time.time()

    FAILURES = []

    print("Reading metadata record")
    g = Graph()
    get_party_from_metadata_record(
        Config.DATASET,
        fix_namespace(Config.DATASET_NAMESPACE),
        Config.METADATA_URI_1,
        Config.OUTPUT_DIR,
        Config.OUTPUT_RDF_FORMAT,
        g,
    )
    get_party_from_metadata_record(
        Config.DATASET,
        fix_namespace(Config.DATASET_NAMESPACE),
        Config.METADATA_URI_2,
        Config.OUTPUT_DIR,
        Config.OUTPUT_RDF_FORMAT,
        g,
    )
    write_to_disk(
        g, "", Config.OUTPUT_DIR, "party_role_citation", rdf_format=Config.OUTPUT_RDF_FORMAT
    )
    print("Reading metadata record completed")

    try:
        sc = SparkContext()
        spark = (
            SparkSession.builder.master(Config.APP_MASTER).appName(Config.APP_NAME).getOrCreate()
        )

        TABLES = Config.tables

        if Config.RELOAD_MAPPINGS:
            upload_vocabulary_mappings(
                spark,
                Config.MAPPING_CSV,
                Config.DATASET,
                "r_vocabulary_mappings",
                mode="overwrite",
            )

        # Get and broadcast the Controlled Vocabularies for the dataset
        vocabs_g = spark.sparkContext.broadcast(
            parse_vocab_from_url(
                parse_vocab_from_url(
                    TernRdf.Graph([AUSPLOTS_FOREST_BINDINGS]),
                    Config.VOCABS[0],
                ),
                Config.VOCABS[1],
            )
        )

        ontology_graph = load_ontology(Config.ONTOLOGIES)
        ontology = spark.sparkContext.broadcast(ontology_graph)

        for table_name in TABLES:
            errors = spark.sparkContext.accumulator([], ListAccumulator())
            warnings = spark.sparkContext.accumulator([], ListAccumulator())

            print(
                f"-- {table_name} ----------------------------------------------------------------------------------"
            )
            table_start_time = time.time()

            try:
                mod = __import__("transform_tables.{}".format(table_name), fromlist=["Table"])
                table = mod.Table(spark)
                table_transform = table.transform
                table.clean()
                table.validate()
                table.pre_transform(errors, warnings)
                vocab_mappings = table.load_vocabulary_mappings()
                lookup = table.load_lookup()

                if hasattr(table, "perform_transform"):
                    if not table.perform_transform:
                        print(
                            "{} transform job finished in {:.2f} seconds".format(
                                table_name, time.time() - table_start_time
                            )
                        )
                        print("-------------\n")
                        continue

                perform_transform(
                    table,
                    table_transform,
                    table.dataset,
                    table.namespace,
                    table_name,
                    vocab_mappings,
                    vocabs_g,
                    errors,
                    warnings,
                    lookup,
                    ontology,
                )
                if len(errors.value) > 0:
                    FAILURES.append(table_name)
            except Exception as e:
                traceback.print_exception(type(e), e, e.__traceback__)
                FAILURES.append(table_name)

            print(
                "{} transform job finished in {:.2f} seconds".format(
                    table_name, time.time() - table_start_time
                )
            )
            print("-------------\n")

        print(
            "\n-- Output ----------------------------------------------------------------------------------"
        )
        print("Total Spark job finished in {:.2f} seconds".format(time.time() - start_time))

    except Exception as e:
        traceback.print_exception(type(e), e, e.__traceback__)
        FAILURES.append("init ETL")

    if FAILURES:
        with open(os.path.join(Config.APP_DIR, "FAILURE"), "w") as fp:
            pass
        print("Failed tables: {}".format(FAILURES))
    else:
        # Creating a file at specified location
        with open(os.path.join(Config.APP_DIR, "SUCCESS"), "w") as fp:
            pass
        print("No failures.")
