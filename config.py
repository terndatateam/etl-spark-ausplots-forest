import os

import numexpr
from tern_rdf.namespace_bindings import AUSPLOTS_FOREST_DATASET

try:
    from dotenv import load_dotenv

    load_dotenv()
except:
    pass


class Config:
    APP_DIR = os.path.dirname(os.path.realpath(__file__))
    OUTPUT_DIR = "output"
    OUTPUT_RDF_FORMAT = "turtle"

    # Sets a name for the application, which will be shown in the Spark web UI.
    # If no application name is set, a randomly generated name will be used.
    APP_NAME = "AUSPLOTS-FORESTS-SPARK-ETL"

    DATASET = "ausplots_forests"
    DATASET_NAMESPACE = AUSPLOTS_FOREST_DATASET

    METADATA_URI_1 = "0e503109-2fb6-4182-969f-2d570abdbabd"
    METADATA_URI_2 = "1dd61f70-7fc8-495f-8c88-823e2834b10b"

    # Sets the Spark master URL to connect to, such as "local" to run locally, "local[4]" to run locally with 4 cores,
    # or "spark://master:7077" to run on a Spark standalone cluster.
    CPU_COUNT = numexpr.detect_number_of_cores()
    print(f"Executing ETL with {CPU_COUNT} cores/threads")
    APP_MASTER = f"local[{CPU_COUNT}]"

    # Select the tables to process commenting and uncommenting them
    tables = [
        "data_study_location",  # ok
        "r_largetreesurvey",  # ok
        "r_speciesdeterminations",  # ok
        "transect_quadrats",  # ok
        "fuel_sitevisit",  # ok
        "fuel_fuelbedcover",  # ok  # ok
        "fuel_woodydebrisstock",  # ok
        "fuel_plantheight",  # ok
        "fuel_plantlitter",  # ok
        "fuel_fuelbiomassandmoistureandfuelload",  # ok
        "fuel_duffdepth",  # ok
        "fuel_coarseandfinewoodydebrislitterfall",  # ok
    ]

    ufoi_list = {
        "local_government_areas_2011": [
            "http://linked.data.gov.au/dataset/local-gov-areas-2011/",
            "lga_code11",
        ],
        "capad_2018_terrestrial": [
            "http://linked.data.gov.au/dataset/capad-2018-terrestrial/",
            "pa_pid",
        ],
        "ibra7_regions": ["http://linked.data.gov.au/dataset/bioregion/", "reg_code_7"],
        "ibra7_subregions": [
            "http://linked.data.gov.au/dataset/bioregion/",
            "sub_code_7",
        ],
        "nrm_regions": ["http://linked.data.gov.au/dataset/nrm-2017/", "nrm_id"],
        "states_territories": [
            "http://linked.data.gov.au/dataset/asgs2016/stateorterritory/",
            "state_code",
        ],
        "wwf_terr_ecoregions": [
            "http://linked.data.gov.au/dataset/wwf-terr-ecoregions/",
            "objectid",
        ],
    }

    # Force the execution to re-download the Taxa files and re-index them.
    # This may take a long time
    force_taxa_index = False

    # APNI and APC urls (for taxa tables)
    APNI = "http://linked.data.gov.au/dataset/apni"
    APC = "http://linked.data.gov.au/dataset/apc"

    GENERALISATION_REPORT = "https://sds.ala.org.au/sensitive-species-data.xml"
    CONSERVATION_STATUS_LIST = os.path.join(APP_DIR, "conservation_status_report_11102022.csv")

    # Choose the currently accepted national taxonomy needed for the project
    taxas = [
        "APNI/APC",
        "AusMoss",
        # 'Fungi',
        # 'Lichen'
    ]

    HERBARIUM_FILES = "taxa_files"

    # Source for vocabularies and ontologies
    VOCABS = [
        "https://graphdb.tern.org.au/repositories/ausplots_vocabs_core/statements",
        "https://graphdb.tern.org.au/repositories/tern_vocabs_core/statements",
    ]

    ONTOLOGIES = [
        "https://raw.githubusercontent.com/ternaustralia/ontology_tern/master/docs/tern.ttl",
        "https://w3id.org/tern/ontologies/loc.ttl",
    ]

    RELOAD_MAPPINGS = True
    MAPPING_CSV = "https://docs.google.com/spreadsheets/d/1Zon-3em4OLUoPwCm4KAK2I_VRmDsu8zt4Fm6zYsNGKc/export?format=csv&gid=488310777"

    # ---------- SHACL Validation --------------------------------------------------------------------------------------

    # SHACL Shape file path
    SHACL_SHAPE = "./shacl/plot-ontology-shacl.ttl"

    # --------- DEBUG SETTINGS -----------------------------------------------------------------------------------------

    # Set to true to execute only in one core (even if the spark job has started with more cores)
    run_on_single_process = False

    # Setting this variable to True will filter the source dataset with the indicated IDs
    DEBUG = False

    # IDs to achieve a complete Graph for 3 different Site Visits
    DEBUG_SITE_LOCATION_VISIT_ID = [
        "VCFSEH0004-1",
        "NSFSEC0005-1",
        "VCFSEH0002-1",
        "VCFSEH0002-2",
        "QDWET0002-1",
        "WAFWAR0009-1",
    ]
    DEBUG_SITE_LOCATION_ID = [
        "VCFSEH0004",
        "NSFSEC0005",
        "VCFSEH0002",
        "QDWET0002",
        "WAFWAR0009",
    ]
